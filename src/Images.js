import Image01 from './memsImages/01.jpg';
import Image02 from './memsImages/02.jpg';
import Image03 from './memsImages/03.jpg';
import Image04 from './memsImages/04.jpg';
import Image05 from './memsImages/05.jpg';
import Image06 from './memsImages/06.jpg';
import Image07 from './memsImages/07.jpg';
import Image08 from './memsImages/08.jpg';
import Image09 from './memsImages/09.jpg';
import Image10 from './memsImages/10.jpg';
import Image11 from './memsImages/11.jpg';
import Image12 from './memsImages/12.jpg';
import Image13 from './memsImages/13.jpg';
import Image14 from './memsImages/14.jpg';
import Image15 from './memsImages/15.jpg';
import Image16 from './memsImages/16.jpg';
import Image17 from './memsImages/17.jpg';
import Image18 from './memsImages/18.jpg';

export default [Image01, Image02, Image03, Image04, Image05, Image06, Image07, Image08, Image09, Image10,
                Image11, Image12, Image13, Image14, Image15, Image16, Image17, Image18];
